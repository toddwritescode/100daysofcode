/* tabs */
function openWeek(weekNum){
	var iterator, tabs, tabContent;
	tab = document.getElementsByClassName("tab");
	for(iterator = 0; iterator < tab.length; iterator++){
		tab[iterator].className = tab[iterator].className.replace(" selected", "");
	}
	tabContent = document.getElementsByClassName("tab-content");
	for (iterator = 0; iterator < tabContent.length; iterator++) {
		tabContent[iterator].className = tabContent[iterator].className.replace(" selected", "");
	}
	document.getElementById("week-"+weekNum+"-tab").className += " selected";
	document.getElementById("week-"+weekNum+"-content").className += " selected";
}

/* button routing */
function buttonRouting(route){
	window.location.href = '../100daysofcode/' + route;
}