	$(document).ready(function(){
    $('#typewriteText')
    .on('typewriteStarted', function() {
        console.log('typewrite has started');
    })
    .on('typewriteComplete', function() {
        console.log('typewrite has complete');
    })
    .on('typewriteTyped', function(event, data) {
        console.log('typewrite has typed', data);
    })
    .on('typewriteRemoved', function(event, data) {
        console.log('typewrite has removed', data);
    })
    .on('typewriteNewLine', function() {
        console.log('typewrite has added a new line');
    })
    .on('typewriteSelected', function(event, data) {
        console.log('typewrite has selected text', data);
    })
    .on('typewriteDelayEnded', function() {
        console.log('typewrite delay has ended');
    })
    .typewrite({
        showCursor: false,
        actions: [
            {type: 'I am developer.'},
            {delay: 1500},
            {select: {from: 5, to: 16}},
            {remove: {num: 10, type: 'whole'}},
            {type: 'passionate.'},
            {delay: 1500},
            {select: {from: 5, to: 17}},
            {remove: {num: 11, type: 'whole'}},
            {type: 'unique.'},
            {delay: 1500},
            {remove: {num: 12, type: 'stepped'}},
            {type: 'toddwritescode.'}
        ]
    });
});