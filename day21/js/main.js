function toggleMenuButtons() {
    var openButton = document.getElementById('open'),
        closeButton = document.getElementById('close');

    if (openButton.style.display === "none") {
        openButton.style.display = "block";
        closeButton.style.display = "none";
    } else {
        openButton.style.display = "none";
        closeButton.style.display = "block";
    }
}

function startWandering() {
    var openButton = document.getElementById('open'),
        closeButton = document.getElementById('close');

    openButton.style.display = "none";
    closeButton.style.display = "block";
}